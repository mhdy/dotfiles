# add all directories in ~/.local/bin to PATH
paths="$(find ~/.local/bin -type d -printf %p:)"
export PATH="$PATH:${paths%%:}"

# man pages path
export MANPATH="$HOME/.local/share/man:$(man -w)"

# default programs
export EDITOR="vim"
export TERMINAL="st"
export BROWSER="firefox"

# load any local bash profile
[ -f "$HOME/.bash_profile_local" ] && source "$HOME/.bash_profile_local"

# start ui
ssh-agent startx
