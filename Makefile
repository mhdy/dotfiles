clean:
	@echo nothing to clean

bin:
	cp -rf .local/bin ~/.local/

install:
	install -m 0600 .bash_profile .xinitrc .bashrc .bash_aliases ~/
	cp -rf .config ~/
	cp -rf .local ~/

uninstall:
	@echo remove manually

local:
	install -m 600 .bash_profile_local .bash_local .bluetoothdb .wifidb .bookmarks ~/

.PHONY: clean install uninstall local
