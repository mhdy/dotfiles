alias \
  tree='tree -I .git -a -l -F --dirsfirst --charset=ascii' \
  grep='grep --color=auto' \
  python='python3' \
  z='setsid -f zathura' \
  ze='setsid -f zathura main.pdf && exit' \
  zv='setsid -f zathura main.pdf && vim main.tex' \
  kbr='xset r rate 300 70 && xmodmap -e "keysym Alt_R = Multi_key"' \
  xcp='xclip -selection clipboard' \
  shred='shred -f -z -n 3 -u' \
  csv='column -t -s' \
  cal='cal -3' \
  mps='ps --forest -u $UID -o pid,user,command' \
  rsync='rsync -av --progress' \
  myip='ip="$(curl --silent https://wtfismyip.com/text)"; echo "$ip"; echo "$ip" | xclip -sel c'
